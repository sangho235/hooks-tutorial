import React from 'react';
import Info from './Info';

const App = () => {
  //주석1 체리픽 테스트
  //주석2 브런치보다 2단계 앞서야지 태그도 달아야지
  //주석3 태그는 gitlab에서 달아야지 v0.0.3?
  return <Info/>;
}

export default App;